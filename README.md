![pandora logo](./assets/logo.png)

# Pandora

O projeto Pandora é uma caixa automatizada para recebimento e armazenamento de encomendas em residências, visando aprimorar a conveniência e a praticidade da logística em compras online. Com o avanço do comércio digital, torna-se crucial avaliar os impactos e as novas demandas dos consumidores nesse contexto. O desafio abordado é a necessidade de disponibilidade para receber as entregas ou retirá-las em agências, afetando a experiência do usuário em marketplaces.

O sistema consiste em uma caixa de entregas Pandora e um aplicativo de acompanhamento Pandora. O sistema emite um comprovante ao entregador e notifica o proprietário da caixa sobre a recepção da encomenda. O acesso e retirada das encomendas são realizados pelo usuário através de uma porta exclusiva, proporcionando uma gestão eficiente do recebimento de encomendas.


# Pandora-FrontEnd-App

Este repositório consiste no desenvolvimento do frontend do aplicativo mobile da Pandora, a ser utilizado pelo usuário. Utilizando React e Expo como framework, proporcionando uma experiência de desenvolvimento ágil e eficiente para a criação de interfaces interativas.

## Tecnologias Utilizadas

- React Native: Framework para construção de aplicativos móveis nativos com base em React.
- Expo: Plataforma que simplifica o desenvolvimento de aplicativos móveis nativos.

O uso do Expo neste projeto oferece uma maneira simplificada e eficiente de desenvolver um frontend móvel, permitindo que os desenvolvedores aproveitem as vantagens do React Native com menos complexidade na configuração e acesso a recursos nativos do dispositivo.

## Configuração do Ambiente

### Pré-requisitos

Antes de começar, certifique-se de que você tenha o [Node.js](https://nodejs.org/) e o [Expo CLI](https://docs.expo.dev/get-started/installation/) instalados em seu sistema.

### Etapas

1.  Clone o repositório para o seu ambiente local:

    `git clone https://gitlab.com/fga-pi2/2023-2/grupo01/pandora-frontend-app.git`

2.  Navegue até o diretório do projeto:

    `cd pandora-frontend-app`

3.  Instale as dependências do projeto:

    `npm install`

### Rodando o Projeto

1.  De volta ao terminal, execute o seguinte comando para iniciar o projeto:

    `npm start`

2.  O Expo abrirá uma página no seu navegador com um código QR.

3.  Instale o aplicativo Expo Go no seu dispositivo móvel (disponível na App Store ou Google Play).

4.  Abra o aplicativo Expo Go e escaneie o código QR exibido no seu navegador.

5.  O aplicativo será carregado no seu dispositivo móvel para teste.

Observação: Certifique-se de que seu dispositivo móvel e o computador estejam conectados à mesma rede Wi-Fi.

## Desenvolvedores


|![Sofia Patrocinio](https://gitlab.com/uploads/-/system/user/avatar/2742933/avatar.png?width=96)|![Irwin Schmitt](https://gitlab.com/uploads/-/system/user/avatar/4603371/avatar.png?width=96)|![Leonardo da Silva Gomes](https://gitlab.com/uploads/-/system/user/avatar/2998542/avatar.png?width=96)|
|---|---|---|
|Sofia Patrocinio|Irwin Schmitt|Leonardo Gomes|
