import { Navigation } from './src/navigation/Navigation';
import { NavigationContainer } from '@react-navigation/native';
import { OrderContextProvider, CategoryContextProvider } from './src/contexts';
import { createStackNavigator } from "@react-navigation/stack";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect } from 'react';

const Stack = createStackNavigator();

export default function App() {
  return (
    <OrderContextProvider>
      <CategoryContextProvider>
        <NavigationContainer>
          <Navigation />
        </NavigationContainer>
      </CategoryContextProvider>
    </OrderContextProvider>
  );
}

// const Wrapper = () => {
//   const { user, logout } = useAuth();

//   return (
//       <Stack.Navigator>
//         {user ? (
//           <Stack.Screen
//             name="Order"
//             component={OrderNavigation}
//             options={{
//               headerRight: () => <Button title="Sair" onPress={logout} />,
//             }}
//           />
//         ) : (
//           <>
//             <Stack.Screen name="Login" component={LoginScreen} />
//             <Stack.Screen name="Register" component={RegisterScreen} />
//           </>
//         )}
//       </Stack.Navigator>
//   );
// };
