import React, {useEffect, useState} from 'react';
import AsyncStorage from "@react-native-async-storage/async-storage";

import { createStackNavigator } from '@react-navigation/stack';
import { OrdersScreen, CreateOrderScreen, CreateCategoryScreen, OrderDetailsScreen, CategoryDetailsScreen, RegisterScreen, LoginScreen } from '../screens'

const Stack = createStackNavigator();

export function Navigation() {
  const [token, setToken] = useState();

  useEffect(async () => {
    let accessToken = await AsyncStorage.getItem("@pandora:auth_token");
    console.log(accessToken["token"])
    setToken(accessToken)
  }, [])

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      {token ? (
        <>
          <Stack.Screen name="Orders" component={OrdersScreen} />
          <Stack.Screen name="CreateOrder" component={CreateOrderScreen} />
          <Stack.Screen name="CreateCategory" component={CreateCategoryScreen} />
          <Stack.Screen name="CategoryDetails" component={CategoryDetailsScreen} />
          <Stack.Screen name="OrderDetails" component={OrderDetailsScreen} />
        </>
      ) : (
        <>
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Register" component={RegisterScreen} />
          <Stack.Screen name="Orders" component={OrdersScreen} />
          <Stack.Screen name="CreateOrder" component={CreateOrderScreen} />
          <Stack.Screen name="CreateCategory" component={CreateCategoryScreen} />
          <Stack.Screen name="CategoryDetails" component={CategoryDetailsScreen} />
          <Stack.Screen name="OrderDetails" component={OrderDetailsScreen} />
        </> 
      )}
    </Stack.Navigator>
  );
}
