import { createContext, useContext, useEffect, useState } from "react";
import * as SecureStore from "expo-secure-store";

import api from "services/api";

const TOKEN_KEY = "token";
const AuthContext = createContext({});

export const useAuth = () => {
  return useContext(AuthContext);
};

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [token, setToken] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const loadUser = async () => {

      const token = await SecureStore.getItemAsync(TOKEN_KEY);


      if (!token) {
        setLoading(false);
        return;
      }

      api.defaults.headers["Authorization"] = `Bearer ${token}`;

      setToken(token);

      const { data } = await api.get(`/users/me`);

      console.log("user", user);

      setUser(data);
      setLoading(false);
    };

    loadUser();
  }, []);

  const register = async (name, email, phone, password) => {
    try {
      await api.post(`$/users`, {
        name,
        email,
        phone,
        password,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const login = async (email, password) => {
    try {
      const { data } = await api.post(
        `/auth/login`,
        { username: email, password },
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
        }
      );

      console.log("login response", data);

      await SecureStore.setItemAsync(TOKEN_KEY, data.access_token);

      api.defaults.headers["Authorization"] = `Bearer ${data.access_token}`;

      setToken(data.access_token);

      const { data: user } = await api.get(`/users/me`);

      console.log(user);

      setUser(user);

      console.log(user);
    } catch (error) {
      console.log(error);
    }
  };

  const logout = async () => {
    await SecureStore.deleteItemAsync(TOKEN_KEY);

    api.defaults.headers["Authorization"] = "";

    setUser(null);
    setToken(null);
  };

  const values = {
    user,
    token,
    register,
    login,
    logout,
  };

  return (
    <AuthContext.Provider value={values}>
      {!loading && children}
    </AuthContext.Provider>
  );
};
