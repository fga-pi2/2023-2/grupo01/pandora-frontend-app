import React, { createContext, useState, useEffect, useContext } from 'react';
import { OrderService } from '../services';

const OrderContext = createContext();

export function OrderContextProvider({ children }) {
  const [orders, setOrders] = useState([]);
  const [order, setOrder] = useState([]);
  const [isLoadingOrders, setIsLoadingOrders] = useState(false);
  const [isLoadingOrder, setIsLoadingOrder] = useState(false);

  async function getAllOrders() {
    try {
      setIsLoadingOrders(true);
      const response = await OrderService.getAllOrders(1);
      setOrders(response);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoadingOrders(false);
    }
  }

  async function getOrderDetails(order_id) {
    try {
      setIsLoadingOrder(true);
      const response = await OrderService.getOrderDetails(1, order_id);
      setOrder(response);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoadingOrder(false);
    }
  }

  useEffect(() => {
    getAllOrders();
  }, []);

  return (
    <OrderContext.Provider value={{ getAllOrders, getOrderDetails, orders, order, isLoadingOrders, isLoadingOrder }}>
      {children}
    </OrderContext.Provider>
  );
}

export const useOrders = () => useContext(OrderContext);
