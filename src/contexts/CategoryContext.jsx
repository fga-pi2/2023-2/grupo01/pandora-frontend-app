import React, { createContext, useState, useEffect, useContext } from 'react';
import { CategoryService } from '../services';

const CategoryContext = createContext();

export function CategoryContextProvider({ children }) {
  const [categories, setCategories] = useState([]);
  const [isLoadingCategories, setIsLoadingCategories] = useState(false);

  async function getAllCategories() {
    try {
      setIsLoadingCategories(true);
      const response = await CategoryService.getAllCategories();
      setCategories(response);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoadingCategories(false);
    }
  }

  useEffect(() => {
    getAllCategories();
  }, []);

  return (
    <CategoryContext.Provider value={{ getAllCategories, categories, isLoadingCategories }}>
      {children}
    </CategoryContext.Provider>
  );
}

export const useCategories = () => useContext(CategoryContext);
