import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, Switch, ActivityIndicator, Alert } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import styles from './styles';
import { CategoryService } from '../../services';
import { generateRandomPastelColors } from '../../utils';
import { useCategories, useOrders } from '../../contexts';

export function CategoryDetailsScreen({ route, navigation }) {
    const categoryId = route.params.categoryId;
    const { category, isLoadingCategory, getAllCategories } = useCategories();
    const [categoryDetails, setCategoryDetails] = useState(category);
    const [name, setName] = useState('');
    const [selectedColor, setSelectedColor] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [pastelColors, setPastelColors] = useState(generateRandomPastelColors());
    const { getAllOrders } = useOrders();

    useEffect(() => {
        getCategoryDetails();
    }, []);

    const getCategoryDetails = async () => {
        try {
            const response = await CategoryService.getCategoryDetails(categoryId);
            setCategoryDetails(response);
            setName(response.name);
            setSelectedColor(response.color);
        } catch (error) {
            console.log(error);
        }
    };

    const handleSaveCategory = async () => {
        setIsLoading(true);

        const categoryData = JSON.stringify({
            name,
            color: selectedColor,
        });

        try {
            await CategoryService.updateCategory(categoryId, categoryData);
            setIsLoading(false);
            Alert.alert('Sucesso', 'Categoria atualizada com sucesso.');
            await getAllCategories();
            navigation.goBack();
        } catch (error) {
            console.error('Erro ao atualizar categoria:', error);
            Alert.alert('Erro', 'Não foi possível atualizar a categoria. Tente novamente.');
        }
    };

    const handleDeleteCategory = async () => {
        setIsLoading(true);

        Alert.alert(
            'Confirmação',
            'Tem certeza que deseja excluir esta categoria?',
            [
                {
                    text: 'Cancelar',
                    style: 'cancel',
                },
                {
                    text: 'Excluir',
                    onPress: async () => {
                        try {
                            await CategoryService.deleteCategory(categoryId);
                            setIsLoading(false);
                            Alert.alert('Sucesso', 'Categoria excluída com sucesso.');
                            await getAllCategories();
                            navigation.goBack();
                        } catch (error) {
                            console.error('Erro ao excluir categoria:', error);
                            Alert.alert('Erro', 'Não foi possível excluir a categoria. Tente novamente.');
                        }
                    },
                    style: 'destructive',
                },
            ],
            { cancelable: false }
        );
    };

    if (isLoadingCategory || !categoryDetails) {
        return (
            <ActivityIndicator size="large" color="#384173" />
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <AntDesign name="arrowleft" size={24} color="#384173" />
                </TouchableOpacity>
            </View>

            <View>
                <Text style={styles.title}>Detalhes da Categoria</Text>
            </View>

            <View style={styles.content}>
                <Text style={styles.label}>Nome</Text>
                <TextInput
                    style={styles.input}
                    value={name}
                    onChangeText={(text) => setName(text)}
                    editable
                />
            </View>

            <View style={styles.color}>
                <Text style={styles.label}>Cor</Text>
                <View style={styles.colorContainer}>
                    {pastelColors.map((color, i) => (
                        <TouchableOpacity
                            key={i}
                            style={[
                                styles.colorCircle,
                                {
                                    backgroundColor: color,
                                    opacity: selectedColor === color ? 0.5 : 1,
                                    borderColor: selectedColor === color ? '#000' : '#FFF',
                                },
                            ]}
                            onPress={() => setSelectedColor(color)}
                        />
                    ))}
                </View>
            </View>

            <View style={styles.content}>
                <TouchableOpacity onPress={handleDeleteCategory}>
                    <Text style={styles.link}>Deletar Categoria</Text>
                </TouchableOpacity>
            </View>

            <TouchableOpacity style={styles.createButton} onPress={handleSaveCategory}>
                {isLoading ? (
                    <ActivityIndicator size="small" color="#fff" />
                ) : (
                    <Text style={styles.createButtonText}>Salvar</Text>
                )}
            </TouchableOpacity>
        </View>
    );
}

export default CategoryDetailsScreen;
