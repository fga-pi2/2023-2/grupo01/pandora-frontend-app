import React, { useState } from "react";
import { View, TouchableOpacity, Text, Image, ActivityIndicator } from "react-native";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { Input } from "components/Input";

import { ErrorSnackbar } from "../../components/ErrorSnackbar";
import { UserService } from '../../services';
import styles from './styles';

export const LoginScreen = () => {
  const navigation = useNavigation();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleLogin = async () => {
    setLoading(true);
    const response = await UserService.getToken({username: email, password});

    setLoading(false);
    if (response.error) {
      setError(true);
      setTimeout(() => setError(false), 4000);
      return;
    }

    try {
      await AsyncStorage.setItem(
        "@pandora:auth_token",
        response
      );
      navigation.navigate("Orders");
    } catch (e) {
      console.log(e)
      setError(true);
      setTimeout(() => setError(false), 4000);
      return;
    }
    
  };

  return (
    <View style={styles.container}>
      <View style={styles.imageWrapper}>
        <Image
          resizeMode="center"
          source={require("../../assets/logo500.png")}
        />
      </View>

      <Input value={email} placeholder="E-mail" onChangeText={setEmail} />

      <Input
        value={password}
        placeholder="Senha"
        secureTextEntry
        onChangeText={setPassword}
      />

      <TouchableOpacity style={styles.button} onPress={handleLogin}>
        {loading ? (
          <ActivityIndicator size="small" color="#fff" />
        ): (
          <Text style={styles.buttonText}>Entrar</Text>
        )}
      </TouchableOpacity>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          marginTop: 16,
        }}
      >
        <Text>Não possui conta? </Text>

        <Text
          style={styles.link}
          onPress={() => {
            setLoading(false)
            navigation.navigate("Register")
          }}
        >
          Cadastre-se aqui
        </Text>
      </View>

      <ErrorSnackbar visible={error} title="Error no login" description="Tente novamente"/>
    </View>
  );
};
