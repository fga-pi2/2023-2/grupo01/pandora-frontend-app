import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    imageWrapper: {
        width: 300,
        height: 300,
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
      },
    container: {
        padding: 32,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff",
    },
    header: {
        fontSize: 24,
        fontWeight: "bold",
    },
    button: {
        marginTop: 16,
        width: "100%",
        backgroundColor: "#384173",
        color: "#fff",
        height: 42,
        borderRadius: 15,
        borderWidth: 0,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    buttonText: {
        color: "#fff",
        fontSize: 20,
    },
    link: {
        fontSize: 14,
        textDecorationLine: "underline",
    },
});

export default styles;