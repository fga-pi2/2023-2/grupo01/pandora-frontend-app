import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, TouchableOpacity, ActivityIndicator, Alert } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import FloatingButton from '../../components/FloatingButton';
import styles from './styles';
import { useOrders } from '../../contexts';
import { OrderService } from '../../services';

export function OrdersScreen({ navigation }) {
  const { orders, isLoadingOrders } = useOrders();
  const [encomendas, setEncomendas] = useState([]);
  const [openIsLoading, setOpenIsLoading] = useState(false);

  useEffect(() => {
    setEncomendas(orders);
  }, [orders]);

  const navigateToOrderDetails = (orderId) => {
    navigation.navigate('OrderDetails', { orderId });
  };

  const navigateToCreateOrder = () => {
    navigation.navigate('CreateOrder');
  };

  const openBox = async () => {
    try {
      setOpenIsLoading(true);
      await OrderService.openBox();
      setOpenIsLoading(false);
      Alert.alert('Sucesso', 'Caixa aberta com sucesso.');
    } catch (error) {
      console.error('Erro ao abrir caixa:', error);
      Alert.alert('Erro', 'Não foi possível abrir a caixa. Tente novamente.');
    }
  };

  return (
    <View style={styles.container}>
      {isLoadingOrders ? (
        <ActivityIndicator size="large" color="#384173" />
      ) : encomendas.length === 0 ? (
        <View style={styles.emptyMessage}>
          <AntDesign name="exclamation" style={{ marginBottom: 16 }} size={85} color="#384173" />
          <Text style={styles.emptyText}>Você não possui encomendas.</Text>
          <TouchableOpacity onPress={() => navigateToCreateOrder()}>
            <Text style={[styles.emptyText, styles.linkText]}>Cadastre para visualizar.</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <>
          <TouchableOpacity style={styles.createButton} onPress={openBox}>
            {openIsLoading ? (
              <ActivityIndicator size="small" color="#fff" />
            ) : (
              <View style={styles.rowContainer}>
                <AntDesign style={{ paddingRight: 10 }} name="inbox" size={24} color="#fff" />
                <Text style={styles.createButtonText}>Abrir Porta de Saída</Text>
              </View>
            )}
          </TouchableOpacity>
          <FlatList
            data={encomendas}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item, index }) => (
              <TouchableOpacity onPress={() => navigateToOrderDetails(item.id)}>
                <View style={[styles.encomendaItem, index % 2 === 1 ? styles.oddOrder : null]}>
                  <View style={styles.rowContainer}>
                    <Text style={styles.nickname}>{item.nickname}</Text>
                    <View style={styles.chipsContainer}>
                      {item.categories.slice(0, 2).map((category, i) => (
                        <View key={i} style={[styles.chip, { backgroundColor: category.color }]}>
                          <Text style={styles.chipLabel}>{category.name}</Text>
                        </View>
                      ))}
                      {item.categories.length > 2 && (
                        <Text style={styles.moreCategoriesText}>{`+${item.categories.length - 2}`}</Text>
                      )}
                    </View>
                  </View>
                  <View style={styles.rowContainer}>
                    <Text style={styles.label}>Código de Rastreio</Text>
                    <Text>{item.tracking_number}</Text>
                  </View>
                  <View style={styles.rowContainer}>
                    <Text style={styles.label}>Status</Text>
                    <Text>{item.status}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </>
      )}
      <FloatingButton />
    </View>
  );
};