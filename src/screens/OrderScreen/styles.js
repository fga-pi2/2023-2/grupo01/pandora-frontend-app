import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        marginTop: 50
    },
    nickname: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 10
    },
    emptyMessage: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    },
    emptyText: {
      fontSize: 20,
      fontWeight: 700
    },
    linkText: {
      textDecorationLine: 'underline'
    },
    encomendaItem: {
        padding: 20,
        height: 120
    },
    oddOrder: {
        backgroundColor: '#E4E4E4'
    },
    label: {
        fontSize: 16
    },
    rowContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    chipsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
      },
      chip: {
        borderRadius: 8,
        paddingVertical: 4,
        paddingHorizontal: 8,
        marginHorizontal: 4,
      },
      chipLabel: {
        color: 'white',
      },
      moreCategoriesText: {
        marginLeft: 8,
      },
      createButton: {
        backgroundColor: '#384173',
        borderRadius: 20,
        padding: 16,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 16,
      },
      createButtonText: {
        color: '#FFF',
        fontSize: 16,
      },
});

export default styles;