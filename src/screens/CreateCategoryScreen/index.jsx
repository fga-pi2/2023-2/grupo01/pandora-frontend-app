import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, Switch, ActivityIndicator, Alert } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import styles from './styles';
import { CategoryService } from '../../services';
import { generateRandomPastelColors } from '../../utils';
import { useCategories } from '../../contexts';

export function CreateCategoryScreen({ navigation }) {
    const [name, setName] = useState('');
    const [selectedColor, setSelectedColor] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [pastelColors, setPastelColors] = useState(generateRandomPastelColors());
    const { getAllCategories } = useCategories();

    const handleCreateCategory = async () => {
        setIsLoading(true);
        const categoryData = JSON.stringify({
            name,
            color: selectedColor,
        });

        try {
            await CategoryService.createCategory(categoryData);
            setIsLoading(false);
            await getAllCategories();
            Alert.alert('Sucesso', 'Categoria criada com sucesso.');
            navigation.goBack();
        } catch (error) {
            console.error('Erro ao criar categoria:', error);
            Alert.alert('Erro', 'Não foi possível criar a categoria. Tente novamente.');
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <AntDesign name="arrowleft" size={24} color="#384173" />
                </TouchableOpacity>
            </View>

            <View>
                <Text style={styles.title}>Nova Categoria</Text>
            </View>

            <View style={styles.content}>
                <Text style={styles.label}>Nome da Categoria</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Ex.: Vestuário"
                    value={name}
                    onChangeText={(text) => setName(text)}
                />
            </View>

            <View style={styles.color}>
                <Text style={styles.label}>Cor</Text>
                <View style={styles.colorContainer}>
                    {pastelColors.map((color, i) => (
                        <TouchableOpacity
                            key={i}
                            style={[
                                styles.colorCircle,
                                {
                                    backgroundColor: color,
                                    opacity: selectedColor === color ? 0.5 : 1,
                                    borderColor: selectedColor === color ? '#000' : '#FFF',
                                },
                            ]}
                            onPress={() => setSelectedColor(color)}
                        />
                    ))}
                </View>
            </View>

            <TouchableOpacity style={styles.createButton} onPress={handleCreateCategory}>
                {isLoading ? (
                    <ActivityIndicator size="small" color="#fff" />
                ) : (
                    <Text style={styles.createButtonText}>Criar</Text>
                )}
            </TouchableOpacity>
        </View >
    );
};

export default CreateCategoryScreen;
