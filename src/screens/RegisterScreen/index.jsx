import React, { useState } from "react";
import { View, TouchableOpacity, Text, Image, ActivityIndicator } from "react-native";

import { Input } from "components/Input";
import { useNavigation } from "@react-navigation/native";

import { ErrorSnackbar } from "../../components/ErrorSnackbar";
import { SuccessSnackbar } from "../../components/SuccessSnackbar";
import { UserService } from '../../services';
import styles from './styles';

export const RegisterScreen = () => {
  const navigation = useNavigation();

  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleRegister = async () => {
    setLoading(true);

    const response = await UserService.createUser({name, email, phone, password});

    setLoading(false);

    if (response.error) {
      setError(true);
      setTimeout(() => setError(false), 4000);
      return;
    }

    setSuccess(true);
    setTimeout(() => {
      setSuccess(false);
      navigation.navigate("Login");
    }, 2000);
  };

  return (
    <View style={styles.container}>
      <View style={styles.imageWrapper}>
        <Image
          resizeMode="center"
          source={require("../../assets/logo500.png")}
        />
      </View>

      <Input value={name} placeholder="Nome" onChangeText={setName} /> 
      <Input value={email} placeholder="E-mail" onChangeText={setEmail} />
      <Input value={phone} placeholder="Telefone" onChangeText={setPhone} />

      <Input
        value={password}
        placeholder="Senha"
        secureTextEntry
        onChangeText={setPassword}
      />

      <TouchableOpacity style={styles.button} onPress={handleRegister}>
        {loading ? (
          <ActivityIndicator size="small" color="#fff" />
        ) : (
          <Text style={styles.buttonText}>Cadastrar</Text>
        )}
      </TouchableOpacity>

      <Text
        style={{
          marginTop: 16,
        }}
      >
        Ao criar uma conta você concorda com nossos
      </Text>

      <Text style={styles.link}>Termos de Uso</Text>

      <ErrorSnackbar visible={error} title="Error no cadastro" description="Tente novamente"/>
      <SuccessSnackbar visible={success} title="Sucesso" description="Cadastro concluido"/>
    </View>
  );
};
