import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  imageWrapper: {
    width: 300,
    height: 300,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    padding: 32,
  },
  header: {
    fontSize: 24,
    fontWeight: "bold",
  },
  input: {
    borderWidth: 1,
    borderColor: "#ccc",
    padding: 10,
    margin: 10,
  },
  button: {
    marginTop: 16,
    width: "100%",
    backgroundColor: "#384173",
    color: "#fff",
    height: 42,
    borderRadius: 15,
    borderWidth: 0,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: "#fff",
    fontSize: 20,
  },
});

export default styles;