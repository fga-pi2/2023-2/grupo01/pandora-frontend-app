import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  header: {
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2',
    marginTop: 30,
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
    paddingLeft: 14
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#384173',
    textAlign: 'center',
    padding: 16
  },
  content: {
    padding: 16,
  },
  input: {
    backgroundColor: '#F2F2F2',
    borderRadius: 20,
    marginBottom: 16,
    padding: 12,
  },
  chipsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 16,
    alignItems: 'center'
  },
  categoryChip: {
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: '#384173',
    borderRadius: 8,
    paddingVertical: 8,
    paddingHorizontal: 16,
    margin: 4,
  },
  categoryName: {
    color: '#384173',
  },
  selectedCategoryChip: {
    borderColor: '#C9C9C9'
  },
  selectedCategoryName: {
    color: '#C9C9C9',
  },
  createButton: {
    backgroundColor: '#384173',
    borderRadius: 20,
    padding: 16,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 16,
  },
  createButtonText: {
    color: '#FFF',
    fontSize: 16,
  },
  notification: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  button: {
    backgroundColor: '#384173',
    width: 36,
    height: 36,
    borderRadius: 28,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 12,
    marginRight: 4,
    fontSize: 20
  },
});

export default styles;