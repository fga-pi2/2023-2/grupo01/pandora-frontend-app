import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, Alert, ActivityIndicator } from 'react-native';
import { LongPressGestureHandler, State } from 'react-native-gesture-handler';
import { AntDesign } from '@expo/vector-icons';
import styles from './styles';
import { OrderService } from '../../services';
import { useCategories, useOrders } from '../../contexts';

export function CreateOrderScreen({ navigation }) {
    const { categories, isLoadingCategories } = useCategories();
    const [categorias, setCategorias] = useState([]);
    const [trackingCode, setTrackingCode] = useState('');
    const [description, setDescription] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [selectedCategoryIds, setSelectedCategoryIds] = useState([]);
    const { getAllOrders } = useOrders();

    useEffect(() => {
        setCategorias(categories);
    }, [categories]);

    const handleCreateOrder = async () => {
        setIsLoading(true);
        const box_id = 1;
        const orderData = JSON.stringify({
            tracking_number: trackingCode,
            nickname: description,
            categories: selectedCategoryIds,
        });

        try {
            await OrderService.createOrder(box_id, orderData);
            setIsLoading(false);
            await getAllOrders();
            Alert.alert('Sucesso', 'Encomenda criada com sucesso.');
            navigation.navigate('Orders');
        } catch (error) {
            console.error('Erro ao criar encomenda:', error);
            Alert.alert('Erro', 'Não foi possível criar a encomenda. Tente novamente.');
        }
    };

    const toggleCategorySelection = (categoryId) => {
        const isSelected = selectedCategoryIds.includes(categoryId);

        if (isSelected) {
            setSelectedCategoryIds((prevIds) => prevIds.filter((id) => id !== categoryId));
        } else {
            setSelectedCategoryIds((prevIds) => [...prevIds, categoryId]);
        }
    };

    const handlePress = () => {
        navigation.navigate('CreateCategory');
    };

    const handleLongPress = (categoryId) => {
        navigation.navigate('CategoryDetails', { categoryId });
    };

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <AntDesign name="arrowleft" size={24} color="#384173" />
                </TouchableOpacity>
            </View>

            <View>
                <Text style={styles.title}>Nova Encomenda</Text>
            </View>

            <View style={styles.content}>
                <Text style={styles.label}>Código de Rastreio</Text>
                <TextInput
                    style={styles.input}
                    placeholder="AA012345678BR"
                    value={trackingCode}
                    onChangeText={(text) => setTrackingCode(text)}
                />

                <Text style={styles.label}>Descrição</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Ex.: Vestido"
                    value={description}
                    onChangeText={(text) => setDescription(text)}
                />

                <Text style={styles.label}>Categorias</Text>
                <View style={styles.chipsContainer}>
                    <TouchableOpacity style={styles.button} onPress={handlePress}>
                        <AntDesign name="plus" size={16} color="white" />
                    </TouchableOpacity>

                    {isLoadingCategories ? (
                        <ActivityIndicator size="small" color="#fff" />
                    ) : (
                        categorias.map((category) => (
                            <TouchableOpacity
                                onPress={() => { toggleCategorySelection(category.id); }}
                                onLongPress={() => { handleLongPress(category.id) }}
                                style={[
                                    styles.categoryChip,
                                    selectedCategoryIds.includes(category.id) && styles.selectedCategoryChip,
                                ]}
                            >
                                <Text style={[styles.categoryName, selectedCategoryIds.includes(category.id) && styles.selectedCategoryName]}>{category.name}</Text>
                            </TouchableOpacity>
                        ))
                    )}
                </View>

                {/* <View style={styles.notification}>
                    <Text style={{ marginLeft: 8 }}>Notificar Entrega</Text>
                    <Switch
                        value={deliveryNotification}
                        onValueChange={(value) => setDeliveryNotification(value)}
                    />
                </View> */}
            </View>

            <TouchableOpacity style={styles.createButton} onPress={handleCreateOrder}>
                {isLoading ? (
                    <ActivityIndicator size="small" color="#fff" />
                ) : (
                    <Text style={styles.createButtonText}>Cadastrar</Text>
                )}
            </TouchableOpacity>
        </View>
    );
};

export default CreateOrderScreen;