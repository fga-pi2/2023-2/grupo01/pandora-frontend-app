import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, Alert, ActivityIndicator } from 'react-native';
import { AntDesign, MaterialIcons } from '@expo/vector-icons';
import styles from './styles';
import { useOrders, useCategories } from '../../contexts';
import { OrderService } from '../../services';

export function OrderDetailsScreen({ route, navigation }) {
    const orderId = route.params.orderId;
    const { categories, isLoadingCategories } = useCategories();
    const [categorias, setCategorias] = useState([]);
    const { order, isLoadingOrder } = useOrders();
    const [orderDetails, setOrderDetails] = useState(order);
    const [nickname, setNickname] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [selectedCategoryIds, setSelectedCategoryIds] = useState([]);
    const [isDeleted, setIsDeleted] = useState(false);
    const { getAllOrders } = useOrders();

    useEffect(() => {
        getOrderDetails();
    }, []);

    useEffect(() => {
        setCategorias(categories);
    }, [categories]);

    const getOrderDetails = async () => {
        try {
            const response = await OrderService.getOrderDetails(1, orderId);
            setOrderDetails(response);
            setNickname(response.nickname);
            const categoryIds = response.categories.map(category => category.id);
            setSelectedCategoryIds(categoryIds);
        } catch (error) {
            console.log(error);
        }
    };

    const toggleCategorySelection = (categoryId) => {
        const isSelected = selectedCategoryIds.includes(categoryId);

        if (isSelected) {
            setSelectedCategoryIds((prevIds) => prevIds.filter((id) => id !== categoryId));
        } else {
            setSelectedCategoryIds((prevIds) => [...prevIds, categoryId]);
        }
    };

    const formatDate = (dateString) => {
        const options = { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' };
        return new Date(dateString).toLocaleDateString('pt-BR', options);
    }

    const handlePress = () => {
        navigation.navigate('CreateCategory');
    };

    const handleLongPress = (categoryId) => {
        navigation.navigate('CategoryDetails', { categoryId });
    };

    const handleSaveOrder = async () => {
        setIsLoading(true);
        const box_id = 1;
        const orderData = JSON.stringify({
            nickname,
            categories: selectedCategoryIds
        });

        try {
            await OrderService.updateOrder(box_id, orderId, orderData);
            setIsLoading(false);
            await getAllOrders();
            Alert.alert('Sucesso', 'Encomenda atualizada com sucesso.');
            navigation.navigate('Orders');
        } catch (error) {
            console.error('Erro ao atualizar encomenda:', error);
            Alert.alert('Erro', 'Não foi possível atualizar a encomenda. Tente novamente.');
        }
    };

    const handleDeleteOrder = async () => {
        setIsLoading(true);
        const box_id = 1;
        Alert.alert(
            'Confirmação',
            'Tem certeza que deseja excluir esta encomenda?',
            [
                {
                    text: 'Cancelar',
                    style: 'cancel',
                },
                {
                    text: 'Excluir',
                    onPress: async () => {
                        try {
                            setIsDeleted(true);
                            await OrderService.deleteOrder(box_id, orderId);
                            setIsLoading(false);
                            await getAllOrders();
                            Alert.alert('Sucesso', 'Encomenda excluída com sucesso.');
                            navigation.navigate('Orders');
                        } catch (error) {
                            console.error('Erro ao excluir encomenda:', error);
                            Alert.alert('Erro', 'Não foi possível excluir a encomenda. Tente novamente.');
                        }
                    },
                    style: 'destructive',
                },
            ],
            { cancelable: false }
        );
    };

    if (!orderDetails) {
        return (
            <ActivityIndicator size="large" color="#384173" />
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <AntDesign name="arrowleft" size={24} color="#384173" />
                </TouchableOpacity>
            </View>

            <View>
                <Text style={styles.title}>Detalhes da Encomenda</Text>
            </View>

            {orderDetails.delivery_date && (
                <View style={styles.timelineContainer}>
                    <View style={styles.timelineInfoContainer}>
                        <Text style={styles.label}>Pedido Entregue</Text>
                        <Text>{formatDate(orderDetails.delivery_date)}</Text>
                    </View>
                </View>
            )}

            {orderDetails.created_at ? (
                <View style={styles.timelineContainer}>
                    <View style={styles.timelineInfoContainer}>
                        <Text style={styles.label}>Pedido Criado</Text>
                        <Text>{formatDate(orderDetails.created_at)}</Text>
                    </View>
                </View>
            ) : (
                <ActivityIndicator size="small" color="#384173" />
            )}

            <View style={styles.content}>
                <Text style={styles.label}>Código de Rastreio</Text>
                <TextInput
                    style={[styles.input, styles.inputDisabled]}
                    value={orderDetails.tracking_number}
                    editable={false}
                />

                <Text style={styles.label}>Descrição</Text>
                <TextInput
                    style={styles.input}
                    value={nickname}
                    onChangeText={(text) => setNickname(text)}
                    editable
                />

                <Text style={styles.label}>Categorias</Text>
                <View style={styles.chipsContainer}>
                    <TouchableOpacity style={styles.button} onPress={handlePress}>
                        <AntDesign name="plus" size={16} color="white" />
                    </TouchableOpacity>

                    {isLoadingCategories ? (
                        <ActivityIndicator size="small" color="#384173" />
                    ) : (
                        categorias.map((category) => (
                            <TouchableOpacity
                                onPress={() => { toggleCategorySelection(category.id); }}
                                onLongPress={() => { handleLongPress(category.id) }}
                                style={[
                                    styles.categoryChip,
                                    selectedCategoryIds.includes(category.id) && styles.selectedCategoryChip,
                                ]}
                            >
                                <Text style={[styles.categoryName, selectedCategoryIds.includes(category.id) && styles.selectedCategoryName]}>{category.name}</Text>
                            </TouchableOpacity>
                        ))
                    )}
                </View>
            </View>


            <View style={styles.content}>
                <TouchableOpacity onPress={handleDeleteOrder}>
                    <Text style={styles.link}>Deletar Encomenda</Text>
                </TouchableOpacity>
            </View>

            <TouchableOpacity style={styles.createButton} onPress={handleSaveOrder}>
                {isLoading ? (
                    <ActivityIndicator size="small" color="#fff" />
                ) : (
                    <Text style={styles.createButtonText}>Salvar</Text>
                )}
            </TouchableOpacity>
        </View>
    );
}

export default OrderDetailsScreen;
