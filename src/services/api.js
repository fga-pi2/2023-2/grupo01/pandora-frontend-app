import axios from 'axios';
import AsyncStorage from "@react-native-async-storage/async-storage";

const BASE_URL = 'https://pandora-backend-4d46183345f3.herokuapp.com';

const instance = axios.create({
  baseURL: BASE_URL,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json", 
    "Accept": "*/*",
  }
});

instance.interceptors.request.use(
  async (config) => {
    let accessToken = await AsyncStorage.getItem("@pandora:auth_token");
    if (accessToken) {
      config.headers.Authorization = `Bearer ${accessToken}`;
    }
    return config;
  },
  (error) => {
    console.log(error);
  }
);

export default instance;
