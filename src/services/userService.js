import api from "./api";

export class UserService {
  constructor() {}

  static async createUser({ name, password, phone, email }) {
    try {
      const data = {
        name,
        email,
        phone,
        password,
      };

      const response = await api.post("/users/", data);
      return response.data;
    } catch (error) {
      console.error(error);
      return { error: true, message: error };
    }
  }

  static async getUser() {
    try {
      const response = await api.get("/users/me/");
      return response.data;
    } catch (error) {
      console.error(error);
      return { error: true, message: error?.response?.data };
    }
  }

  static async getToken({ username, password }) {
    try {
      const data = {
        username,
        password
      };

      const token = await api.post("/auth/login/", data, 
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      });
      return token.data.access_token;
    } catch (error) {
      console.error(error);
      return { error: true, message: error?.response?.data };
    }
  }
}

