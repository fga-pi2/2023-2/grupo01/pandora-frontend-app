import api from './api';

const url = '/categories';

const headers = {
  "Content-Type": "application/json"
};

export class CategoryService {
  static async getAllCategories() {
    try {
      const response = await api.get(`${url}/`, { headers });
    return response.data;
  } catch(error) {
    console.error(error);
    return { error: true, message: error?.response?.data };
  }
}

  static async createCategory(categoryData) {
  try {
    const response = await api.post(`${url}/`, categoryData, { headers });
    return response.data;
  } catch (error) {
    console.error(error);
    return { error: true, message: error?.response?.data };
  }
}

  static async getCategoryDetails(category_id) {
  try {
    const response = await api.get(`${url}/${category_id}`, { headers });
    return response.data;
  } catch (error) {
    console.error(error);
    return { error: true, message: error?.response?.data };
  }
}

  static async updateCategory(category_id, categoryData) {
  try {
    const response = await api.put(`${url}/${category_id}`, categoryData, { headers });
    return response.data;
  } catch (error) {
    console.error(error);
    return { error: true, message: error?.response?.data };
  }
}

  static async deleteCategory(category_id) {
  try {
    const response = await api.delete(`${url}/${category_id}`, { headers });
    return response.data;
  } catch (error) {
    console.error(error);
    return { error: true, message: error?.response?.data };
  }
}
}
