import api from './api';

const url = '/boxes';

const headers = {
  "Content-Type": "application/json"
};

export class OrderService {
  static async getAllOrders(box_id) {
    try {
      const response = await api.get(`${url}/${box_id}/orders/`, { headers });
      return response.data;
    } catch (error) {
      console.error(error);
      return { error: true, message: error?.response?.data };
    }
  }

  static async createOrder(box_id, orderData) {
    try {
      const response = await api.post(`${url}/${box_id}/orders/`, orderData, { headers });
      return response.data;
    } catch (error) {
      console.error(error);
      return { error: true, message: error?.response?.data };
    }
  }

  static async getOrderDetails(box_id, order_id) {
    try {
      const response = await api.get(`${url}/${box_id}/orders/${order_id}`, { headers });
      return response.data;
    } catch (error) {
      console.error(error);
      return { error: true, message: error?.response?.data };
    }
  }

  static async updateOrder(box_id, order_id, orderData) {
    try {
      const response = await api.put(`${url}/${box_id}/orders/${order_id}`, orderData, { headers });
      return response.data;
    } catch (error) {
      console.error(error);
      return { error: true, message: error?.response?.data };
    }
  }

  static async deleteOrder(box_id, order_id) {
    try {
      const response = await api.delete(`${url}/${box_id}/orders/${order_id}`, { headers });
      return response.data;
    } catch (error) {
      console.error(error);
      return { error: true, message: error?.response?.data };
    }
  }

  static async openBox() {
    try {
      const response = await api.post(`${url}/1/open_door/`, { headers });
      return response.data;
    } catch (error) {
      console.error(error);
      return { error: true, message: error?.response?.data };
    }
  }
}
