import React from 'react';
import { TextInput, View, StyleSheet } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import styles from './styles';

export function Input({ placeholder, value, onChangeText, secureTextEntry, icon }) {
    return (
        <View style={styles.inputContainer}>
          <FontAwesome name={icon} style={styles.icon} />

          <TextInput
            placeholder={placeholder}
            value={value}
            onChangeText={onChangeText}
            style={styles.input}
            secureTextEntry={secureTextEntry}
          />
        </View>
      );
}
