import React from 'react';
import { Entypo } from '@expo/vector-icons';
import styles from './styles';
import { Input } from '../Input';

export function SearchBar({ placeholder, onChange, style }) {
  return (
    <Input
      icon={<Entypo name="magnifying-glass" size={30}/>}
      inputContainerStyle={[styles.searchBar, style]}
      onChange={onChange}
      placeholder={placeholder}
    />
  );
}


