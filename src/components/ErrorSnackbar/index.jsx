import React from 'react';
import { View, Text } from "react-native";

import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import SnackbarComponet from 'react-native-snackbar-component';

import styles from './styles';

export function ErrorSnackbar({ visible, title, description }) {
  return (
    <SnackbarComponet
        visible={visible} 
        textMessage={() => (
         <View style={styles.snackbarWrapper}>
            <MaterialCommunityIcons name="alert-circle" style={styles.iconSnackbar}/>
            <Text style={styles.snackbarBold}>{title}</Text>
            <Text style={styles.snackbar}>{description}</Text>
          </View>
        )}
        messageColor="#000000"
        backgroundColor="#FDE0DB"
        position="top"

    />
  );
}


