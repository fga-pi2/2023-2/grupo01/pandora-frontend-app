import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  snackbarWrapper: {
    display: 'flex',
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",

    marginTop: 40,

    paddingHorizontal: 20,
    paddingVertical: 16
  },
  iconSnackbar: {
    fontSize: 20,
    paddingRight: 12
  },
  snackbarBold: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  snackbar: {
    paddingLeft: 5,
    fontSize: 16,
  }
});

export default styles;

