module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    plugins: [
      [
        "module-resolver",
        {
          alias: {
            contexts: "./src/contexts",
            components: "./src/components",
            navigation: "./src/navigation",
            screens: "./src/screens",
            services: "./src/services",
          },
        },
      ],
    ],
  };
};
